using System;
using System.Collections.Generic;
using GildedRoseKata;
using NUnit.Framework;

namespace GildedRoseTests
{
    public class Tests
    {
        private const int ItemQualityDecreaseRate = 1;
        private const int ConjuredItemQualityDecreaseRate = 2;
        private const int ItemMaxQuality = 50;

        [SetUp]
        public void Setup()
        {
        }

        [Test]
        [TestCase(10)]
        [TestCase(5)]
        [TestCase(0)]
        public void SimpleItemQualityDecreaseAfterUpdate(int quality)
        {
            IList<Item> items = new List<Item> {new Item {Name = "foo", SellIn = 10, Quality = quality}};
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = Math.Clamp(quality - ItemQualityDecreaseRate, 0, int.MaxValue);
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(100)]
        [TestCase(50)]
        [TestCase(0)]
        [TestCase(-10)]
        public void SimpleItemSellInDecreaseAfterUpdate(int sellIn)
        {
            IList<Item> items = new List<Item> {new Item {Name = "foo", SellIn = sellIn, Quality = 10}};
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = sellIn - 1;
            Assert.AreEqual(expected, items[0].SellIn);
        }

        [Test]
        [TestCase(-1, 10)]
        [TestCase(-10, 0)]
        public void ItemQualityIsReducedTwiceAsFastWhenSellInIsLowerThanZero(int sellIn, int quality)
        {
            IList<Item> items = new List<Item> {new Item {Name = "foo", SellIn = sellIn, Quality = quality}};
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = Math.Clamp(quality - 2 * ItemQualityDecreaseRate, 0, int.MaxValue);
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(10, 10)]
        [TestCase(10, 0)]
        public void AgedBrieIncreaseInQualityTheOlderItGets(int sellIn, int quality)
        {
            IList<Item> items = new List<Item> {new Item {Name = "Aged Brie", SellIn = sellIn, Quality = quality}};
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = quality + ItemQualityDecreaseRate;
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(-10, 10)]
        [TestCase(-10, 0)]
        public void AgedBrieIncreaseInQualityTwiceAsFastAfterSellDate(int sellIn, int quality)
        {
            IList<Item> items = new List<Item> {new Item {Name = "Aged Brie", SellIn = sellIn, Quality = quality}};
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = quality + 2 * ItemQualityDecreaseRate;
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(15, 50)]
        public void ItemQualityCantGoOver50(int sellIn, int quality)
        {
            IList<Item> items = GetAgedBrie(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            Assert.AreEqual(ItemMaxQuality, items[0].Quality);
        }

        [Test]
        [TestCase(15, 30)]
        [TestCase(-10, 30)]
        [TestCase(-10, 40)]
        [TestCase(10, 20)]
        public void LegendaryItemDontDecreaseInQuality(int sellIn, int quality)
        {
            IList<Item> items = GetALegendaryItem(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            Assert.AreEqual(quality, items[0].Quality);
        }

        [Test]
        [TestCase(15, 10)]
        [TestCase(-10, 20)]
        [TestCase(-10, 30)]
        [TestCase(10, 40)]
        public void LegendaryItemDontDecreaseInSellinDate(int sellIn, int quality)
        {
            IList<Item> items = GetALegendaryItem(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            Assert.AreEqual(sellIn, items[0].SellIn);
        }

        [Test]
        [TestCase(50, 50)]
        [TestCase(25, 30)]
        [TestCase(20, 40)]
        [TestCase(15, 20)]
        public void BackStagePassQualityIncreaseWhenNotCloseToSellDate(int sellIn, int quality)
        {
            IList<Item> items = GetABackStagePass(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = Math.Clamp(quality + 1, 0, ItemMaxQuality);
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(3, 30)]
        [TestCase(2, 30)]
        [TestCase(1, 50)]
        public void BackStagePassQualityIncreaseThriceAsFastIfSellinDateIsLessOrEqualToFive(int sellIn, int quality)
        {
            IList<Item> items = GetABackStagePass(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = Math.Clamp(quality + 3, 0, ItemMaxQuality);
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(6, 30)]
        [TestCase(8, 20)]
        [TestCase(10, 50)]
        public void BackStagePassQualityIncreaseTwiceAndFastWhenSellinDateIsBetweenFiveAndTen(int sellIn, int quality)
        {
            IList<Item> items = GetABackStagePass(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = Math.Clamp(quality + 2, 0, ItemMaxQuality);
            Assert.AreEqual(expected, items[0].Quality);
        }

        [Test]
        [TestCase(0, 30)]
        [TestCase(-1, 20)]
        [TestCase(-10, 50)]
        public void BackStagePassQualityGoesToZeroIfSellinDateIsPassed(int sellIn, int quality)
        {
            IList<Item> items = GetABackStagePass(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            Assert.AreEqual(0, items[0].Quality);
        }   
        
        /// <summary>
        /// Here we assume that other items can't also be conjured : no such thing as a Conjured Aged Brie or a Conjured Sulfuras
        /// </summary>
        /// <param name="sellIn"></param>
        /// <param name="quality"></param>
        [Test]
        [TestCase(10, 30)]
        [TestCase(5, 30)]
        [TestCase(0, 30)]
        [TestCase(-10, 30)]
        
        public void ConjuredItemDropInQualityTwiceAsFast(int sellIn, int quality)
        {
            IList<Item> items = GetAConjuredItem(sellIn, quality);
            var app = new GildedRose(items);

            app.UpdateQuality();

            var expected = quality - ConjuredItemQualityDecreaseRate * ( sellIn > 0 ? 1 : 2 );
            Assert.AreEqual( expected , items[0].Quality);
        }
        
        private static List<Item> GetAgedBrie(int sellIn, int quality)
        {
            return new List<Item> {new Item {Name = GildedRose.AgedBrie, SellIn = sellIn, Quality = quality}};
        }

        private static List<Item> GetALegendaryItem(int sellIn, int quality)
        {
            return new List<Item>
                {new Item {Name = GildedRose.SulfurasHandOfRagnaros, SellIn = sellIn, Quality = quality}};
        }

        private static List<Item> GetABackStagePass(int sellIn, int quality)
        {
            return new List<Item>
                {new Item {Name = GildedRose.BackstagePassesToATafkal80EtcConcert, SellIn = sellIn, Quality = quality}};
        }        
        
        private static List<Item>GetAConjuredItem(int sellIn, int quality)
        {
            return new List<Item>
                {new Item {Name = GildedRose.ConjuredManaCake, SellIn = sellIn, Quality = quality}};
        }
    }
}