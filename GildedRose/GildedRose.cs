﻿using System.Collections.Generic;

namespace GildedRoseKata
{
    public class GildedRose
    {
        public const string BackstagePassesToATafkal80EtcConcert = "Backstage passes to a TAFKAL80ETC concert";
        public const string AgedBrie = "Aged Brie";
        public const string SulfurasHandOfRagnaros = "Sulfuras, Hand of Ragnaros";
        public const string ConjuredManaCake = "Conjured Mana Cake";

        private readonly IInventoryUpdater _inventoryUpdater;
        private readonly IList<Item> _items;

        public GildedRose(IList<Item> items)
        {
            _items = items;
            _inventoryUpdater = new GildedRoseInventoryUpdater(); // Ideally inject this for more abstraction
        }

        public void UpdateQuality()
        {
            _inventoryUpdater.UpdateInventory(_items);
        }
    }
}