﻿using System.Collections.Generic;
using GildedRoseKata.ItemUpdaters;

namespace GildedRoseKata
{
    public class GildedRoseInventoryUpdater : IInventoryUpdater
    {
        public void UpdateInventory(IEnumerable<Item> items)
        {
            foreach (var item in items)
                switch (item.Name)
                {
                    case GildedRose.BackstagePassesToATafkal80EtcConcert:
                    {
                        new BackStagePassUpdater().UpdateItem(item);
                        break;
                    }
                    case GildedRose.AgedBrie:
                    {
                        new AgedBrieUpdater().UpdateItem(item);
                        break;
                    }
                    case GildedRose.SulfurasHandOfRagnaros:
                    {
                        new SulfurasUpdater().UpdateItem(item);
                        break;
                    }
                    case GildedRose.ConjuredManaCake:
                    {
                        new ConjuredItemUpdater().UpdateItem(item);
                        break;
                    }
                    default:
                    {
                        new NormalItemUpdater().UpdateItem(item);
                        break;
                    }
                }
        }
    }
}