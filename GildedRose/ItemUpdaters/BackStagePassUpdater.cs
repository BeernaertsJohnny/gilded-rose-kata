﻿using System;
using GildedRoseKata.ItemUpdaters.Interface;

namespace GildedRoseKata.ItemUpdaters
{
    public class BackStagePassUpdater : IItemUpdater
    {
        // Could benefit from having an option class setting up values like MaxItemQuality, ItemDregradationRate, etc..
        private const int MaxItemQuality = 50;

        public void UpdateItem(Item item)
        {
            if (item.SellIn <= 0)
                item.Quality = 0;
            else if (item.SellIn < 6)
                item.Quality += 3;
            else if (item.SellIn < 11)
                item.Quality += 2;
            else if (item.SellIn >= 11) item.Quality += 1;

            item.Quality = Math.Clamp(item.Quality, 0, MaxItemQuality);


            item.SellIn -= 1;
        }
    }
}