﻿using System;
using GildedRoseKata.ItemUpdaters.Interface;

namespace GildedRoseKata.ItemUpdaters
{
    public class ConjuredItemUpdater : IItemUpdater
    {
        private const int MaxItemQuality = 50;
        
        public void UpdateItem(Item item)
        {
            item.Quality -= item.SellIn <= 0 ? 4 : 2;
            item.Quality = Math.Clamp(item.Quality, 0, MaxItemQuality);
            item.SellIn -= 1;
        }
    }
}