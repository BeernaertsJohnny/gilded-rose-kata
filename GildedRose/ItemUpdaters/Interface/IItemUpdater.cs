﻿namespace GildedRoseKata.ItemUpdaters.Interface
{
    public interface IItemUpdater
    {
        void UpdateItem(Item item);
    }
}