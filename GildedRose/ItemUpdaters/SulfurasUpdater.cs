﻿using GildedRoseKata.ItemUpdaters.Interface;

namespace GildedRoseKata.ItemUpdaters
{
    public class SulfurasUpdater : IItemUpdater
    {
        public void UpdateItem(Item item)
        {
            // Not affected by updates.
        }
    }
}