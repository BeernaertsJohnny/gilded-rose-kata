﻿using System;
using GildedRoseKata.ItemUpdaters.Interface;

namespace GildedRoseKata.ItemUpdaters
{
    public class NormalItemUpdater : IItemUpdater
    {
        // Could benefit from having an option class setting up values like MaxItemQuality, ItemDregradationRate, etc..
        private const int MaxItemQuality = 50;

        public void UpdateItem(Item item)
        {
            item.Quality -= item.SellIn <= 0 ? 2 : 1;
            item.Quality = Math.Clamp(item.Quality, 0, MaxItemQuality);
            item.SellIn -= 1;
        }
    }
}