﻿using System.Collections.Generic;

namespace GildedRoseKata
{
    public interface IInventoryUpdater
    {
        void UpdateInventory(IEnumerable<Item> items);
    }
}